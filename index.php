<?php

	if($_SERVER[HTTP_HOST] != "yellzone.xyz")
		header('Location: https://yellzone.xyz');

	/* Configuration de l'affichage des erreurs */
	/*if (!ini_get('display_errors')) {
    	ini_set('display_errors', '1');
	}*/

	/* Les classes dont on aura besoin */
	require 'Classes/Position.php';
	require 'Classes/Ball.php';

	/* Initialisation d'une balle test */
	$ball = new Ball;
	$ball->setxPos(10);
	$ball->setyPos(18);
	$ball->setGoingRight(true);
 ?>
<!DOCTYPE html>
<html>
<head>
	<title> Yell-Zone </title>
	<link href="https://fonts.googleapis.com/css?family=Spinnaker" rel="stylesheet">
	<link rel="stylesheet" href="css/app.css" type="text/css" />
</head>
<body class="page" scrolling="no" background="img/background.jpg">
<br><br>
	<h2 class="title" id="title"> Yell-Zone </h2>
	<h4 class="title-2">Powered by <a href="http://www.medteck-softwares.com" target="_blank" rel="nofollow"> Medteck Softwares </a> </h4>
	<div id="title-3-container">

	</div>
	<iframe src="view.html" class="ifView" id="ifView"></iframe>

	<script type="text/javascript">
		var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
		var height = (window.innerHeight > 0) ? window.innerHeight : screen.height;
		if(height < 500 || width < 800 || height > width) {
			document.getElementById('ifView').style.display= "none";
			document.getElementById('title-3-container').innerHTML = "<br><br> <h4 class='title-3'>Ouvrez votre PC!</h4>";
			document.getElementById('title-3-container').innerHTML += "<h5 class='title-4'>Il vous faut: "+
				"<ul> <li>Un clavier</li> <li>Un ecran d'au moins 800px X 500px, large</li> </ul>"+
				" pour jouer à Yellzone..<br> Rendez-vous sur votre PC pour tenter l'expérience ;-)</h4>";
			document.getElementById('title').style.fontSize= "70px";
		}
	</script>
</body>
</html>
