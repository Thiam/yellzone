<?php 
header('Access-Control-Allow-Origin: *');

	if(isset($_REQUEST["device"]) && isset($_REQUEST["event"]) && isset($_REQUEST["ballYPosition"]) && isset($_REQUEST["ballDx"]) && isset($_REQUEST["ballDy"])) {
		$event = $_REQUEST["event"];
		$ballYPosition = $_REQUEST["ballYPosition"];
		$ballDx = $_REQUEST["ballDx"];
		$ballDy = $_REQUEST["ballDy"];
		$device = $_REQUEST["device"];

		if($ballDx<0)
			$forDevice = $device-1;
		else
			$forDevice = $device+1;

  		$myFile = fopen("../json/balls.js","r+") or die("Erreur: Les balles sont introuvables!");
  		$myFileContent = fread($myFile, filesize("../json/balls.js"));
  		$newBall= ',{"event": "'.$event.'", "ballY": "'.$ballYPosition.'", "ballDx": "'.$ballDx.'", "ballDy":"'.$ballDy.'", "forDevice": "'.$forDevice.'"}';
  		$newFileContent = substr($myFileContent, 0, filesize("../json/balls.js") - 1).$newBall."]";
  		echo("New line: ".$newBall);
  		file_put_contents("../json/balls.js", $newFileContent);
  		fclose($myfile);
	}