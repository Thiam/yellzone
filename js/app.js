document.getElementById("inputKeyboard").addEventListener("click", function(e) {
    if(e.target.value=="Francais") {
        document.getElementById("lup").innerHTML = "A";
        document.getElementById("ldown").innerHTML = "Q";
        document.getElementById("rup").innerHTML = "P";
        document.getElementById("rdown").innerHTML = "M";
    }
    else {
        document.getElementById("lup").innerHTML = "Q";
        document.getElementById("ldown").innerHTML = "A";
        document.getElementById("rup").innerHTML = "P";
        document.getElementById("rdown").innerHTML = "L";
    }
});


function getChar(event) {
  if (event.which == null) {
    return String.fromCharCode(event.keyCode) // IE
  } else if (event.which!=0 && event.charCode!=0) {
    return String.fromCharCode(event.which)   // the rest
  } else {
    return null // special key
  }
}

/*document.addEventListener("keydown",function(){
	if(document.getElementById("rightBar") != null && document.getElementById("leftBar") != null) {
		document.getElementById("rightBar").style.top = 10;
		document.getElementById("leftBar").style.top = 10;
		console.log(document.getElementById("rightBar").style.top);
		console.log(document.getElementById("leftBar").style.top);
	}
});*/

function keyEvent(event) {
	var key = event.keyCode || event.which;
	var keychar = String.fromCharCode(key);
	if(document.getElementById("rightBar") != null && document.getElementById("leftBar") != null) {
		document.getElementById("rightBar").style.margintop = 10;
		document.getElementById("leftBar").style.margintop = 10;
	}
}

function ballOut(position, ballY, ballDx, ballDy, ipServeur) {
    // Create our XMLHttpRequest object
    var hr = new XMLHttpRequest();
    // Create some variables we need to send to our PHP file
    var url = "http://"+ipServeur+"/yellzone/actions/put.php";
    var vars = "device="+position+"&event=ballOut&ballYPosition="+ballY+"&ballDx="+ballDx+"&ballDy="+ballDy+"";
    hr.open("POST", url, true);
    // Set content type header information for sending url encoded variables in the request
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // Access the onreadystatechange event for the XMLHttpRequest object
    hr.onreadystatechange = function() {
	    if(hr.readyState == 4 && hr.status == 200) {
		    var return_data = hr.responseText;
			//document.getElementById("serverResponses").innerHTML += return_data;
	    }
    }
    // Send the data to PHP now... and wait for response to update the status div
    hr.send(vars); // Actually execute the request
    //document.getElementById("status").innerHTML = "processing...";
}

var lastResponse= ([{"event": "ballOut", "ballY": "20", "ballDx": "2", "ballDy":"2", "forDevice": "test"},{"event": "ballOut", "ballY": "21.075849645305425", "ballDx": "2", "ballDy":"-2", "forDevice": "3"},{"event": "ballOut", "ballY": "133.16654528444633", "ballDx": "2", "ballDy":"2", "forDevice": "3"},{"event": "ballOut", "ballY": "53.188546545570716", "ballDx": "-2", "ballDy":"-2", "forDevice": "1"},{"event": "ballOut", "ballY": "129.16654528444633", "ballDx": "2", "ballDy":"-2", "forDevice": "3"},{"event": "ballOut", "ballY": "33.188546545570716", "ballDx": "-2", "ballDy":"-2", "forDevice": "1"}]);

function getBalls(position, ipServeur) {
    var AJAX_req = new XMLHttpRequest();
    AJAX_req.open("GET", "http://"+ipServeur+"/yellzone/json/balls.js", true);
    AJAX_req.setRequestHeader("Content-type", "application/json");

    AJAX_req.onreadystatechange = function()
    {
        if(AJAX_req.readyState==4 && AJAX_req.status==200)
        {
            var response = JSON.parse(AJAX_req.responseText);
            //console.log(response[0]);
            for(var i=0 ; i < response.length ; i++){
                if(response[i].forDevice==position){
                    if(response[i].ballDx>0) {
                        lx = -lballRadius;
                        ly = response[i].ballY;
                        ldx = response[i].ballDx;
                        ldy = response[i].ballDy;
                    }
                    else {
                        rx = -rballRadius;
                        ry = response[i].ballY;
                        rdx = response[i].ballDx;
                        rdy = response[i].ballDy;
                    }
                }
            }
        }
        else
            console.log("Pas de reponse!");
    }
    AJAX_req.send();
}

function authenticate(user, ipServeur, position) {
    // Create our XMLHttpRequest object
    var hr = new XMLHttpRequest();
    // Create some variables we need to send to our PHP file
    var url = "http://"+ipServeur+"/yellzone/authenticate.php";
    var vars = "device="+position+"&event=authenticate&user="+user+"&ipServeur="+ipServeur+"&position="+position;
    hr.open("POST", url, true);
    // Set content type header information for sending url encoded variables in the request
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // Access the onreadystatechange event for the XMLHttpRequest object
    hr.onreadystatechange = function() {
        if(hr.readyState == 4 && hr.status == 200) {
            var response = hr.responseText;
            //document.getElementById("serverResponses").innerHTML += return_data;
        }
    }
    // Send the data to PHP now... and wait for response to update the status div
    hr.send(vars); // Actually execute the request
    //document.getElementById("status").innerHTML = "processing...";
}
