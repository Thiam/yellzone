var leftCanvas = document.getElementById("leftCanvas");
var lctx = leftCanvas.getContext("2d");
var lballRadius = 7;
var lx = Math.random()* (leftCanvas.width - lballRadius*2 - leftCanvas.width/4) + lballRadius*2;
var ly = Math.random()* (leftCanvas.height - lballRadius*3) + lballRadius*3;
var ldx = 2;
var ldy = -2;
var lpaddleHeight = 35;
var lpaddleWidth = 5;
var lpaddleY = (leftCanvas.height-lpaddleHeight)/2;
var lUpPressed = false;
var lDownPressed = false;

var rightCanvas = document.getElementById("rightCanvas");
var rctx = rightCanvas.getContext("2d");
var rballRadius = 7;
var rx = Math.random()* (rightCanvas.width - rballRadius*2) + rballRadius*2 + rightCanvas.width/4;
var ry = Math.random()* (rightCanvas.height - rballRadius*3) + rballRadius*3;
var rdx = -2;
var rdy = -2;
var rpaddleHeight = 35;
var rpaddleWidth = 5;
var rpaddleY = (rightCanvas.height-rpaddleHeight)/2;
var rUpPressed = false;
var rDownPressed = false;

var score= 0;
var lcount = 0;
var rcount = 0;

var loose = false;

var user = "";
var ipServer ="";
var position ="";

var buttons = {
    'left': {
        'up': 81,
        'down': 65
    },
    'right': {
        'up': 80,
        'down': 76
    }
};

var gamePaused = false;


function drawPaddle() {
    lctx.beginPath();
    lctx.rect(leftCanvas.width-lpaddleWidth - 1, lpaddleY,  lpaddleWidth, lpaddleHeight);
    lctx.fillStyle = "#FFD700";
    lctx.fill();
    lctx.closePath();

    rctx.beginPath();
    rctx.rect(1, rpaddleY, rpaddleWidth, rpaddleHeight);
    rctx.fillStyle = "#FFD700";
    rctx.fill();
    rctx.closePath();
}

function drawBall() {
    lctx.beginPath();
    lctx.arc(lx, ly, lballRadius, 0, Math.PI*2);
    lctx.fillStyle = "#FFD700";
    lctx.fill();
    lctx.closePath();

    rctx.beginPath();
    rctx.arc(rx, ry, rballRadius, 0, Math.PI*2);
    rctx.fillStyle = "#FFD700";
    rctx.fill();
    rctx.closePath();
}

function draw() {

    if(loose || gamePaused)
        return;

	/*** Partie Gauche ***/
    lctx.clearRect(0, 0, leftCanvas.width, leftCanvas.height);
    drawBall();
    if(lx + ldx < lballRadius && ldx<0) {
    	ldx = -ldx;
    	ballOut(position,ly,ldx,ldy,ipServer);
	}
	else if(lx + ldx > leftCanvas.width-lballRadius - lpaddleWidth) {
    	if(ly > lpaddleY && ly < lpaddleY + lpaddleHeight) {
        	score = score +1;
        	lcount++;
    		document.getElementById("score").innerHTML = score;
        	if(lcount % 2 == 0){
				document.getElementById("perigo").style.transform = "rotate(90deg)";
        	}
			else{
				document.getElementById("perigo").style.transform = "rotate(90deg)";
			}
        	ldx = -ldx;
    	}
    	else if(lx + ldx > leftCanvas.width+lballRadius) {
    		if(!loose) {
                document.getElementById("perigo").src = "/img/death.png";
                document.getElementById("perigo").className = "gameover";
                document.getElementById("middleSide").innerHTML += '<br> <a href="/" target="_parent"> <input type="button" value="Rejouer" id="butReplay" class="submitButton"> </a>';
            }
        		//alert("GAME OVER");
    		loose=true;
        	//document.location.reload();
    	}
	}

	if((ly + ldy > leftCanvas.height-lballRadius && ldy > 0) || (ly + ldy < lballRadius && ldy < 0))
    	ldy = -ldy;

	ly += ldy;
	lx += ldx;

	if(lUpPressed && lpaddleY-2 > 0) {
    	lpaddleY -= 4;
	}
	else if(lDownPressed && lpaddleY + lpaddleHeight + 2 <leftCanvas.height ) {
    	lpaddleY += 4;
	}

	/*** Partie Droite ***/
    rctx.clearRect(0, 0, rightCanvas.width, rightCanvas.height);
    drawBall();

    if(rx + rdx > rightCanvas.width-rballRadius && rdx>0) {
    	rdx = -rdx;
    	ballOut(position,ry,rdx,rdy,ipServer);
	}
	else if(rx + rdx < rballRadius + rpaddleWidth) {
    	if(ry > rpaddleY && ry < rpaddleY + rpaddleHeight) {
        	rdx = -rdx;
        	score = score +1;
        	rcount++;
    		document.getElementById("score").innerHTML = score;
        	if(rcount % 2 == 0){
				document.getElementById("perigo").style.transform = "rotate(90deg)";
        	}
			else{
				document.getElementById("perigo").style.transform = "rotate(-90deg)";
			}
    	}
    	else if(rx + rdx < -rballRadius) {
    		if(!loose) {
                document.getElementById("perigo").src = "/img/death.png";
                document.getElementById("perigo").className = "gameover";
                document.getElementById("middleSide").innerHTML += '<h2>GAME OVER!</h2> <a href="/" target="_parent"> <input type="button" value="Rejouer" id="butReplay" class="submitButton"> </a>';
            }
        		//alert("GAME OVER");
    		loose=true;
        	//document.location.reload();
    	}
	}

	if(ry + rdy > rightCanvas.height-rballRadius || ry + rdy < rballRadius)
    	rdy = -rdy;

	ry += rdy;
	rx += rdx;

	if(rUpPressed && rpaddleY-2 > 0) {
    	rpaddleY -= 4;
	}
	else if(rDownPressed && rpaddleY + rpaddleHeight + 2 <rightCanvas.height) {
    	rpaddleY += 4;
	}

	drawPaddle();
}

function keyDownHandler(e) {
    if(e.keyCode == buttons.left.up) {
        lUpPressed = true;
    }
    else if(e.keyCode == buttons.left.down) {
        lDownPressed = true;
    }
    if(e.keyCode == buttons.right.up) {
        rUpPressed = true;
    }
    else if(e.keyCode == buttons.right.down) {
        rDownPressed = true;
    }
}

function keyUpHandler(e) {
    if(e.keyCode == buttons.left.up) {
        lUpPressed = false;
    }
    else if(e.keyCode == buttons.left.down) {
        lDownPressed = false;
    }
    if(e.keyCode == buttons.right.up) {
        rUpPressed = false;
    }
    else if(e.keyCode == buttons.right.down) {
        rDownPressed = false;
    }

    if(e.keycode == 13) {
        gamePaused = !gamePaused;
        //if(gamePaused)
            //alert("Paused!");
    }
}

function play() {
	if(document.getElementById("inputNom")!=null && document.getElementById("inputServeur")!=null && document.getElementById("inputPosition")!=null){
		user = document.getElementById("inputNom").value;
		ipServer = document.getElementById("inputServeur").value;
		position = document.getElementById("inputPosition").value;
        keyboard = document.getElementById("inputKeyboard").value;
        if(keyboard == "Francais"){
            buttons = {
                'left': {
                    'up': 65,
                    'down': 81
                },
                'right': {
                    'up': 80,
                    'down': 77
                }
            };
        }
	}
    authenticate(user, ipServer, position);
	document.getElementById("middleSide").innerHTML = '<a id="username"> '+user+' </a> <div class="score" id="score">0</div> <img src="img/perigo22.jpg" class="perigo" id="perigo"> <br> <b style="display:none;" id="paused">PAUSED</b>';
	//setInterval(getBalls(position,ipServer), 50);
    setInterval(draw, 20);
}

document.getElementById("butPlay").addEventListener("click",play);
document.addEventListener("keydown", keyDownHandler, false);
document.addEventListener("keyup", keyUpHandler, false);
