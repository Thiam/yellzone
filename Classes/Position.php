<?php 
	require 'Device.php';
	
	class Position {
		private $x = 0; 			//Définit la position en abscisse de l'élément en question
		private $y = 0; 			//Définit la position en ordonnée de l'élément en question
		private $goingRight; 		//Direction du stuff: True si vers la gauche
		private $device = Device; 			//Définit le numéro de la machine dans laquelle se trouve l'élément en question

		/* Getter et setter pour x */
		function getX() {
			return $this->x;
		}
		function setX($x) {
			$this->x = $x;
		}

		/* Getter et setter pour y */
		function getY() {
			return $this->y;
		}
		function setY($y) {
			$this->y = $y;
		}


	}