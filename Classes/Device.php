<?php
	require 'Dimensions.php';

	class Device {
		private $ipAddress;
		private $dimensions = Dimensions;

		function __construct($ipAddress, $dimensions) {
			$this->ipAddress = $ipAddress;
			$this->dimensions = $dimensions;
    	}

		function getIpAddress() {
			return $this->ipAddress;
		}		
		function setIpAddress($ipAddress) {
			$this->ipAddress = $ipAddress;
		}

		function getdimensions() {
			return $this->dimensions;
		}		
		function setdimensions($dimensions) {
			$this->dimensions = $dimensions;
		}
	}