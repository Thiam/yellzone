<?php 

	class Ball {		
		//private $pos = Position;
		private $xPos;
		private $yPos;
		private $goingRight;
		private $device;
		private $imgPath;

		/* Getter et Setter pour xPos */
		function getxPos() {
			return $this->xpos;
		}
		function setxPos($xpos) {
			$this->xpos = $xpos;
		}

		/* Getter et Setter pour Pos */
		function getyPos() {
			return $this->ypos;
		}
		function setyPos($ypos) {
			$this->ypos = $ypos;
		}

		/* Getter et setter pour la direction */
		function getGoingRight() {
			return $this->goingRight;
		}
		function setGoingRight($goingRight) {
			$this->goingRight = $goingRight;
		}

		/* Getter et setter pour device */
		function getDevice() {
			return $this->device;
		}
		function setDevice($device) {
			$this->device = $device;
		}

		/* Getter et Setter pour pos */
		function getImgPath() {
			return $this->imgPath;
		}
		function setImgPath($imgPath) {
			$this->imgPath = $imgPath;
		}
	}
