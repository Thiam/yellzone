<?php

	class Dimensions {
		private $width; 			//Définit la largeur en pixels de l'écran
		private $height; 			//Définit la hauteur en pixels de l'écran


		/* Constructeurs de la classe */
		function __construct($h, $w) {
			$this->width = $w;
			$this->height = $h;			
    	}

		/* Getter et Setter pour Width */
		function getWidth() {
			return $this->width;
		}
		function setWidth($w) {
			$this->width = $w;
		}

		/* Getter et Setter pour Height */
		function getHeight() {
			return $this->height;
		}
		function setHeight($h) {
			$this->height = $h;
		}

	}